defmodule Stack do
  use GenServer

  # Server
  def init(args) do
    IO.inspect(args)
    {:ok, []}
  end

  def handle_cast({:push, elem}, state) do
    {:noreply, [elem | state]}
  end 

  def handle_call(:pop, _from, state) do
    [h | l] = state
    {:reply, h, l}
  end

  def handle_call(:peek, _from, state) do
    {:reply, state, state}
  end

  # Client
  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def push(elem) do
    GenServer.cast(__MODULE__, {:push, elem})
  end

  def pop() do
    GenServer.call(__MODULE__, :pop)
  end

  def peek() do
    GenServer.call(__MODULE__, :peek)
  end
end
